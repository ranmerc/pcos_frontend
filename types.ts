export interface parametersType {
    age: number;
    marriageStatus: number;
    weight: number;
    bmi: number;
    follicleNoR: number;
    follicleNoL: number;
    AMH: number;
    regularCycle: boolean;
    cycleLength: number;
    skinDarkening: boolean;
    hairGrowth: boolean;
    weightGain: boolean;
    fastFood: boolean;
    pimples: boolean;
}