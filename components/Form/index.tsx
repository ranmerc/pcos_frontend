import { FormEventHandler } from "react";
import InputLabelWrapper from "@/components/InputLabelWrapper";
import TextInput from "@/components/TextInput";

export default function Form() {
  const handleFormSubmit: FormEventHandler = (e) => {
    e.preventDefault();
    console.log(e.target);
  };

  return (
    <>
      <form onSubmit={handleFormSubmit}>
        <InputLabelWrapper
          label="Age"
          InputComponent={<TextInput id="Age" />}
        />
      </form>
    </>
  );
}
