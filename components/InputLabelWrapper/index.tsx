import { HTMLInputTypeAttribute, ReactNode } from "react";

export default function InputLabelWrapper({
  label,
  InputComponent,
}: {
  label: string;
  InputComponent: ReactNode;
}) {
  return (
    <>
      <label htmlFor={label.replace(/\s/, "")}>{label}</label>
      {InputComponent}
    </>
  );
}
