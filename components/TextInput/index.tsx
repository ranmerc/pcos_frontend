export default function TextInput({ id }: { id: string }) {
  return (
    <>
      <input type="text" id={id} />
    </>
  );
}
